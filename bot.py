import asyncio
import json
import logging
import os
import pickle
import re
from typing import List, Mapping

import discord.opus
import jishaku
import praw
from discord.ext.commands import AutoShardedBot, when_mentioned_or, Context
from jishaku.help_command import DefaultPaginatorHelp

logger = logging.getLogger('bot')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='paladins-bot.log', encoding='utf-8', mode='a')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)
logger.info('=== RESTART ===')

with open("options.json") as f:
    options = json.loads(f.read())


class Bot(AutoShardedBot):
    shadowbans: Mapping[int, List[int]] = {}

    def __init__(self, *args, prefix=None, **kwargs):
        super().__init__(prefix, *args, **kwargs)
        self.read_shadowbans()

    def read_shadowbans(self):
        try:
            with open('shadowbans.pkl', 'rb') as f:
                self.shadowbans = pickle.load(f)
        except:
            pass

    async def on_command_error(self, context, exception):
        await context.send(f'{exception}')
        logging.error(exception)

        raise exception

    async def on_member_update(self, before: discord.Member, after: discord.Member):
        settings = self.cogs['Settings'].get_settings_for_guild(after.guild.id)
        if before.nick != after.nick and settings is not None and settings['disallow_nickname_changes']:
            await after.edit(nick=None)

    async def on_member_join(self, member: discord.Member):
        guild: discord.Guild = member.guild
        if guild.id == 555087033652215830:
            chan = 555087033652215836
            rules = 561225575897890826
            about_you = 555088890462076938
        elif guild.id == 609496545569800192:
            chan = 609496545569800194
            rules = 609496663878533130
            about_you = 609496693540651028
        else:
            return

        if chan != -1:
            channel: discord.TextChannel = guild.get_channel(chan)
            await channel.send(f"Hello and welcome! When you get the chance, please read <#{rules}> and post in <#{about_you}>.")

    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        channel: discord.TextChannel = self.get_channel(623223832785846272)
        if payload.cached_message is not None:
            await channel.send(f"Message was deleted: {payload.message_id} "
                               f"by {payload.cached_message.author.display_name} "
                               f"in {payload.cached_message.guild.name} with content"
                               f" `{payload.cached_message.system_content[:1700]}`")
        else:
            await channel.send(f"{payload.message_id} deleted in {(await self.fetch_guild(payload.guild_id)).name}")

    async def on_message(self, msg: discord.Message):
        if not self.is_ready() or msg.author.bot:
            return

        author: discord.Member = msg.author
        if msg.guild.id in self.shadowbans.keys() and author.id in self.shadowbans[msg.guild.id]:
            await msg.delete()
            return

        if "discord.gg" in msg.content and self.cogs['Settings'].get_settings_for_guild(msg.guild.id)['delete_invites']:
            await msg.delete()
            return

        ctx = await self.get_context(msg)

        try:
            await self.process_commands(msg)
            if 'delete_orig' in options and options['delete_orig'] and isinstance(ctx, Context) and ctx.valid:
                await msg.delete()
        except BaseException as e:
            logger.error(repr(e))


# discord.opus.load_opus('/usr/lib/x86_64-linux-gnu/libopus.so.0')

with open('.TOKEN') as f:
    TOKEN = f.read().rstrip()

client = Bot(prefix=when_mentioned_or('!' if 'prefix' not in options else options['prefix']),
             pm_help=True if 'pm_help' not in options else options['pm_help'],
             activity=discord.Game('nothing. Serving Ioun.' if 'game' not in options else options['game']),
             help_command=DefaultPaginatorHelp())


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

for file in os.listdir("cogs"):
    if file.endswith(".py"):
        name = file[:-3]
        client.load_extension(f"cogs.{name}")

client.load_extension("jishaku")

if __name__ == "__main__":
    client.run(TOKEN)
