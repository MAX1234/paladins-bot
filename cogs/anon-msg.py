from discord.ext import commands
import discord


class AnonymousMessaging(commands.Cog):
    def __init__(self, b):
        self.bot = b

    # @commands.command(name='init')
    # async def init(self, ctx: commands.Context, *, u: discord.Member = ''):
    #     """Starts an anonymous connection with a user."""
    #     await ctx.send('Connection started. Use code %s to talk anonymously.' % u.id)

    @commands.command(name='anon', aliases=['anonymous', 'anon-msg'])
    async def anon(self, ctx: commands.Context, u: discord.Member = None, *, msg: str = ''):  # Maybe broken?
        """Sends an anonymous message to a previously opened connection."""
        # msg = message.split(' ')
        # if len(msg) < 2:
        #     return await ctx.send('Error!')

        # user = ctx.guild.get_member(int(msg[0]))

        e = discord.Embed(colour=ctx.author.top_role.colour.value)
        e.set_thumbnail(url=ctx.me.avatar_url)

        e.description = f"You received an anonymous message from {ctx.guild.name}!\nIt said: **{msg}**."

        await u.send(embed=e)
        await ctx.message.delete()


def setup(bot):
    bot.add_cog(AnonymousMessaging(bot))
