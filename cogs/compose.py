import re
import lyricsgenius as genius
import markovify
import json
import os

from discord.ext import commands
import discord


class Compose(commands.Cog):
    artist = "Imagine Dragons"
    TOKEN = 'QcdmhhGJ-kyij_A5We0WOIUk_MLjO_LubxjFylN9BUHAZ9rGlC2Yh_ojMDdNuicn'

    def __init__(self, b):
        self.bot = b

        if not os.path.isfile("Lyrics_ImagineDragons.json"):
            self.download()

        if not os.path.isfile("ID_model.json") or not os.path.isfile("avg_length"):
            Compose.markov()

    def download(self):
        api = genius.Genius(self.TOKEN)
        art = api.search_artist(self.artist)
        art.save_lyrics()

    @staticmethod
    def parse_song(s):
        lyr = s['lyrics']

        return re.sub('\\[.+?\\]', '', lyr)

    @staticmethod
    def markov():
        data = ""

        with open("Lyrics_ImagineDragons.json") as l:
            data = l.read()

        data = json.loads(data)

        dat = []

        text = ""

        for i in data['songs']:
            lyr = Compose.parse_song(i)
            text += lyr
            dat.append(len(lyr.split('\n')) - 1)

        model = markovify.Text(text, state_size=1)

        with open('ID_model.json', 'w') as m:
            model_json = model.to_json()
            m.write(model_json)

        with open('avg_length', 'w') as m:
            m.write(str(sum(dat) / len(dat)))

    @staticmethod
    def make():
        with open('ID_model.json') as m:
            reconstituted_model = markovify.Text.from_json(m.read())

        with open('avg_length') as m:
            l = float(m.read())

        with open("new-song", 'w') as f:
            for i in range(int(l)):
                sent = reconstituted_model.make_sentence()

                if sent:
                    f.write(sent)
                    f.write('\n')
                else:
                    i -= 1

    @commands.command()
    async def compose(self, ctx: commands.Context):
        """Randomly composes a song based on Imagine Dragons songs."""
        Compose.make()
        with open('new-song') as f:
            await ctx.send(file=discord.File(f, filename='New Imagine Dragons Song'))


def setup(bot):
    bot.add_cog(Compose(bot))
