import io
import logging

import discord
from PyLyrics import *
from discord.ext import commands

logging = logging.getLogger('bot')


class Lyrics(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def lyrics(self, ctx: commands.Context, *, text: str):
        """Gets the lyrics of a song, given the artist and name of song."""

        spl = text.split('/')
        if len(spl) < 2:
            return await ctx.send('Not enough args!')

        artist = spl[0]
        name = spl[1]

        lyr = PyLyrics.getLyrics(artist, name)

        if len(lyr) > 1900:
            return await ctx.send(file=discord.File(io.BytesIO(lyr.encode('utf-8'))))

        await ctx.send(lyr)


def setup(bot):
    bot.add_cog(Lyrics(bot))
