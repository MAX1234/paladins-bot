from discord.ext import commands
import discord
from .admin import is_mod, is_admin


class Nuke(commands.Cog):
    def __init__(self, b):
        self.bot = b

    # @commands.command(name='init')
    # async def init(self, ctx: commands.Context, *, u: discord.Member = ''):
    #     """Starts an anonymous connection with a user."""
    #     await ctx.send('Connection started. Use code %s to talk anonymously.' % u.id)

    @commands.command()
    @commands.check(is_admin)
    async def nuke(self, ctx: commands.Context, limit: int):
        await ctx.channel.purge(limit=limit)


def setup(bot):
    bot.add_cog(Nuke(bot))
