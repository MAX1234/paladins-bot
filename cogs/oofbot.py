import logging

import discord
from discord.ext import commands

logging = logging.getLogger('bot')

prefixes = [
    'yotta',
    'zeta',
    'exa',
    'peta',
    'tera',
    'giga',
    'mega',
    'kilo',
    'hecto',
    'deca',
    'deci',
    'centi',
    'milli',
    'micro',
    'nano',
    'pico',
    'femto',
    'atto',
    'zepto',
    'yocto'
]  # Prefixes


class Oof(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def oof(self, ctx: commands.Context, *, lvl: int = -1):
        """Gets the oof level."""
        base = "oof"
        if lvl == -1:
            return await ctx.send('oof')

        if lvl < 1 or lvl > 2 * len(prefixes) - 1:
            return await ctx.send(f'{lvl} is not valid! Do `!ooflvl` for levels.')

        lvl -= 1
        if lvl > len(prefixes) - 1:
            base = "lifegon"
            lvl -= len(prefixes) - 1

        return await ctx.send(f'{prefixes[~lvl]}{base}')

    @commands.command()
    async def ooflvl(self, ctx: commands.Context):
        """Lists all oof levels."""
        embed = discord.Embed()
        embed.colour = discord.Colour(0x8414CC)
        for i in range(2 * len(prefixes) - 1):
            j = i
            if j > len(prefixes) - 1:
                j -= len(prefixes) - 1
            embed.add_field(name=f'{i + 1}', value=f'{prefixes[~j]}{"lifegon" if i > len(prefixes) - 1 else "oof"}')

            if i % 24 == 23:
                await ctx.send(embed=embed)
                embed = discord.Embed()
                embed.colour = discord.Colour(0x8414CC)
        return await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Oof(bot))
