import discord
from discord.ext import commands
import random

class Roll(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def roll(self, ctx, *, text: str):
        dice = text.strip().split('d')
        num = int(dice[0])
        ty = int(dice[1])

        if num == 1:
            await ctx.send(f"Rolled {random.randint(1, ty)}")
        else:
            rolls = [random.randint(1, ty) for i in range(num)]

            await ctx.send(f"Rolled {rolls}, sum {sum(rolls)}")


def setup(bot):
    bot.add_cog(Roll(bot))
