import pickle

from discord.ext import commands
import discord

from cogs import admin

settings_template = {
    'delete_invites': False,
    'disallow_nickname_changes': False
}


class Settings(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.settings = {}
        self.load_settings()

    def get_settings_for_guild(self, guild: int) -> dict:
        if guild in self.settings:
            return self.settings[guild]

    @commands.group()
    async def settings(self, ctx: commands.Context):
        if ctx.invoked_subcommand is None:
            e = discord.Embed(colour=ctx.me.top_role.colour.value)
            s = self.get_settings_for_guild(ctx.guild.id)

            if s is None:
                self.settings[ctx.guild.id] = dict(settings_template)
                self.save_settings()
                s = dict(settings_template)

            for i in zip(s.keys(), s.values()):
                e.add_field(name=i[0], value=str(i[1]), inline=True)
            await ctx.send('', embed=e)

    @settings.command()
    async def query(self, ctx: commands.Context, parametre: str):
        guild: discord.Guild = ctx.guild
        await ctx.send(f"`{parametre}` in {guild.name} is currently set to `{self.get_settings_for_guild(guild.id).get(parametre, 'not found')}`.")

    @settings.command()
    @commands.check(admin.is_mod)
    async def set(self, ctx: commands.Context, setting: str, value: bool):
        guild: discord.Guild = ctx.guild
        self.get_settings_for_guild(guild.id)[setting] = value
        await ctx.send(f"`{setting}` in {guild.name} set to `{value}`.")
        self.save_settings()

    def load_settings(self):
        try:
            with open('settings.pkl', 'rb') as f:
                self.settings = pickle.load(f)
        except:
            self.settings = {}

    def save_settings(self):
        with open('settings.pkl', 'wb') as f:
            pickle.dump(self.settings, f)


def setup(bot):
    bot.add_cog(Settings(bot))
