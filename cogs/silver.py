import collections
import logging
import sqlite3

from discord.ext import commands

conn = sqlite3.connect('db.sqlite3')
cur = conn.cursor()
logging = logging.getLogger('bot')


class Silver(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def silver(self, ctx):
        """Awards a silver to a user."""
        message = ctx.message
        id_ = ""
        spl = message.content.split(' ')
        if len(spl) == 1:
            logging.info('Silver to: ' + str(spl))
            return
        else:
            id_ = ' '.join(spl[1:])

        try:
            ins = cur.execute('INSERT INTO silvers (i) VALUES (?)', (id_,))
            conn.commit()
            await ctx.send('Silver given to %s!' % id_)
            logging.info("Given to %s" % id_)
        except sqlite3.Error as e:
            await ctx.send(str(e))
            logging.error(e)

    @commands.command()
    async def totals(self, ctx):
        """Lists the total silvers given."""
        data = cur.execute('SELECT * FROM silvers;')
        c = collections.Counter()
        for i in data:
            c[i[1]] += 1

        msg = "Totals:\n"
        calc = '\n'.join([
            "%s: %s" % (i, c[i]) for i in c
        ])
        await ctx.send(msg + calc)


def setup(bot):
    # bot.add_cog(Silver(bot))
    """removed cog"""
