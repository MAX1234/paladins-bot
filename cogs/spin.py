import logging
import random

from discord.ext import commands

logging = logging.getLogger('bot')


class Spin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='spin')
    async def spin(self, ctx: commands.Context):
        """Randomly chooses a/b"""
        message = ctx.message
        spl = message.content.split(' ')
        if len(spl) < 2:
            return await ctx.send('Please provide what to spin!')
        spl = spl[1:]
        spl = ' '.join(spl)
        logging.info('Spin: ' + str(spl))
        if '/' in spl:
            s = spl.split('/')
            t1 = s[0]
            t2 = s[1]
        else:
            s = spl.split(' ')
            t1 = s[0]
            t2 = s[1]

        await ctx.send('Chose %s!' % (t1 if random.random() > 0.5 else t2))


def setup(bot):
    bot.add_cog(Spin(bot))
