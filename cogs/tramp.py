import markovify
from discord.ext import commands
import random

with open("trump.txt") as f:
    txt = f.read()

mod = markovify.Text(txt)


class Tramp(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def trump(self, ctx):
        """Prints one sentence generated from Trump's tweets."""
        message = ctx.message
        msg = 'Tramp says: {0}'.format(mod.make_sentence())
        await ctx.send(msg)

    @commands.command()
    async def tweet(self, ctx):
        """Pritns one tweet-length (<=140 char) sentence generated from Trump's tweets."""
        message = ctx.message
        msg = 'Tramp says: {0}'.format(mod.make_short_sentence(140))
        await ctx.send(msg)

    @commands.command(name='paragraph', aliases=['pgf'])
    async def pgf(self, ctx):
        """Generates 5-10 sentences generated from Trump's tweets."""
        message = ctx.message
        msg = 'Tramp says: {0}'.format(' '.join([mod.make_sentence() for i in range(random.randint(5, 10))]))
        await ctx.send(msg)


def setup(b):
    b.add_cog(Tramp(b))
