from yandex.Translater import Translater
from discord.ext import commands


class Trans(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def trans(self, ctx):
        """Translates text
        Usage: !trans <lang (ISO code)> <text>"""
        message = ctx.message
        spl = message.content.split(' ')
        if len(spl) < 3:
            await ctx.send('Not enough args!')
            return

        lang = spl[1]
        sentence = ' '.join(spl[2:])

        tr = Translater()
        tr.set_key('trnsl.1.1.20180308T205203Z.b270ac40c4d1134b.476ecdf0906e19c0b8da79dda859a09cbc40614b')
        tr.set_to_lang(lang)
        tr.set_text(sentence)
        await ctx.send(tr.translate())

    @commands.command()
    async def detect(self, ctx):
        """Detects the language of a string"""
        message = ctx.message
        spl = message.content.split(' ')
        if len(spl) < 2:
            await ctx.send('Not enough args!')
            return

        tr = Translater()
        tr.set_key('trnsl.1.1.20180308T205203Z.b270ac40c4d1134b.476ecdf0906e19c0b8da79dda859a09cbc40614b')
        tr.set_text(' '.join(spl[1:]))
        await ctx.send(tr.detect_lang())

    @commands.command()
    async def langs(self, ctx):
        """Prints supported languages"""
        tr = Translater()
        tr.set_key('trnsl.1.1.20180308T205203Z.b270ac40c4d1134b.476ecdf0906e19c0b8da79dda859a09cbc40614b')
        await ctx.send("I support:\n" + '\n'.join([
            '* %s' % i for i in tr.get_langs()
        ]))


def setup(bot):
    bot.add_cog(Trans(bot))
