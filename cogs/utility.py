import discord
from discord.ext import commands
from tinydb import TinyDB, Query

db = TinyDB('deactivated.json')


def deactivatable(fn):
    async def wrapped(self, ctx, *args, **kwargs):
        query = Query()
        if db.search(query.channel == ctx.message.channel & query.cmd == fn.name):
            return await ctx.send(f'That command is not allowed in {ctx.message.channel.mention}')
        fn(self, ctx, *args, **kwargs)

    return wrapped


class Utility(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def deactivate_in(self, ctx, cmd: str):
        """Deactivates given command in this channel."""
        db.insert({'channel': ctx.message.channel, 'cmd': cmd})

    @commands.command()
    async def reactivate_in(self, ctx, cmd: str):
        """Reactivates given command in this channel."""
        query = Query()
        db.remove(query.channel == ctx.message.channel & query.cmd == cmd)


def setup(b):
    b.add_cog(Utility(b))
