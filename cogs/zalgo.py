import logging

from discord.ext import commands
from zalgo_text import zalgo

logging = logging.getLogger('bot')


class Zalgo(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def zalgo(self, ctx):
        """Applies zalgo to given string"""
        message = ctx.message
        spl = message.content.split(' ')
        logging.info('Zalgo: ' + str(spl))
        if len(spl) < 2:
            await ctx.send('What to Zalgo?')
            return

        await ctx.send(zalgo.zalgo().zalgofy(' '.join(spl[1:])))


def setup(bot):
    bot.add_cog(Zalgo(bot))
